﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GraphQlServer
{
    public interface ICustomerService
    {
        Task<ClientAmendResponse> CreateCustomerAsync(CreateOrEditCustomerRequest request);

        Task<ClientAmendResponse> DeleteCustomerAsync(Guid id);

        Task<ClientAmendResponse> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request);

        Task<List<CustomerShortResponse>> GetCustomersAsync();

        Task<CustomerResponse> GetCustomerAsync(Guid id);
    }
}
