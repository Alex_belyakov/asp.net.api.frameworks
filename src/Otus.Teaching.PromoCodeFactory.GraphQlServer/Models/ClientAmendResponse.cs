﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQlServer
{
    public class ClientAmendResponse
    {
        public Guid Id { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
