﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQlServer
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}