﻿
using System;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer.Client
{
    internal class ClientAmendResponse
    {
        public Guid Id { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
