﻿using System;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer.Client
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}