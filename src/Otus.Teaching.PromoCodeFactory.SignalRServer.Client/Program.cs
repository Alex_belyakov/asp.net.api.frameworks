﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var hubUrl = "http://localhost:5000/hubs/customers";
            var connection = new HubConnectionBuilder()
                .WithUrl(hubUrl)
                .WithAutomaticReconnect()
                .Build();

            await using (connection)
            {
                await connection.StartAsync();

                connection.On("GetCustomersInfo", (List<CustomerShortResponse> customers) =>
                {
                    Console.WriteLine("-------------------Common customers info----------------------");
                    foreach (var customer in customers) 
                    {
                        PrintCustomerShort(customer);
                    }
                });

                connection.On("GetCustomerInfo", (CustomerResponse customer) =>
                {
                    Console.WriteLine($"-------------------Detailed customer {customer.Id} info----------------------");
                    
                        PrintCustomerShort(customer);
                });

                await connection.InvokeAsync("SubscribeCustomersInfo");

                await Task.Delay(3000);

                var myCreateRequest = new CreateOrEditCustomerRequest 
                { 
                    FirstName = "Alex",
                    LastName = "Belyakov",
                    Email = "MyEmail@test", 
                    PreferenceIds = new List<Guid> { new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") }
                };

                var myResponse = await connection.InvokeAsync<ClientAmendResponse>("CreateCustomer", myCreateRequest);

                await connection.InvokeAsync("SubscribeCustomerInfo", myResponse.Id);

                await Task.Delay(3000);

                await connection.InvokeAsync<ClientAmendResponse>("DeleteCustomer", myResponse.Id);

                Console.ReadKey();
            }
        }

        private static void PrintCustomerShort(CustomerShortResponse customer)
        {
            Console.WriteLine($"Customer info:");
            Console.WriteLine($"Id: {customer.Id}");
            Console.WriteLine($"Name: {customer.FirstName} {customer.LastName}");
            Console.WriteLine($"Email: {customer.Email}");
        }
        private static void PrintCustomerShort(CustomerResponse customer)
        {
            var preferences = customer.Preferences.Any() 
                ? customer.Preferences.Select(p => p.Name).Aggregate((p1, p2) => $"{p1}, {p2}") 
                : string.Empty;

            Console.WriteLine($"Customer info:");
            Console.WriteLine($"Id: {customer.Id}");
            Console.WriteLine($"Name: {customer.FirstName} {customer.LastName}");
            Console.WriteLine($"Email: {customer.Email}");
            Console.WriteLine($"Preferences: {preferences}");
        }
    }
}
