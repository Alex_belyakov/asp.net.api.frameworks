﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer
{
    public interface IClientCustomerReceiver
    {
        Task GetCustomerInfo(CustomerResponse customer);
        Task GetCustomersInfo(List<CustomerShortResponse> customers);
    }
}
