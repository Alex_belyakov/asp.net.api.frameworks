﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer
{
    internal class CustomerBackgroundService : BackgroundService
    {
        private readonly IHubContext<CustomersHub, IClientCustomerReceiver> _customersHubContext;
        private readonly ICustomerService _customerService;

        public CustomerBackgroundService(IHubContext<CustomersHub, IClientCustomerReceiver> customersHubContext, ICustomerService customerService)
        {
            _customersHubContext = customersHubContext;
            _customerService = customerService;
        }

        private async Task RefreshCustomersData(List<CustomerShortResponse> customersData)
        {
            await _customersHubContext.Clients.Group(GroupNames.Customers).GetCustomersInfo(customersData);
        }

        private async Task RefreshCustomer(Guid id)
        {
            var groupName = GroupNames.Customer(id);
            var customerData = await _customerService.GetCustomerAsync(id);

            await _customersHubContext.Clients.Group(groupName).GetCustomerInfo(customerData);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    await ExecuteAsync();
                    await Task.Delay(1000, stoppingToken);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Exception occured: " + ex.Message);
            }
        }

        private async Task ExecuteAsync()
        {
            var customersData = await _customerService.GetCustomersAsync();
            var customerIds = customersData.Select(cd => cd.Id);

            await RefreshCustomersData(customersData);

            foreach (var id in customerIds)
            {
                await RefreshCustomer(id);
            }
        }
    }
}
