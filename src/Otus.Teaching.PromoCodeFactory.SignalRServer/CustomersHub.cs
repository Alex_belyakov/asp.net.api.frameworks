﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer
{
    internal class CustomersHub : Hub<IClientCustomerReceiver>
    {
        private readonly ICustomerService _customerService;

        public CustomersHub(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public async Task<ClientAmendResponse> CreateCustomer(CreateOrEditCustomerRequest request)
        {
            return await _customerService.CreateCustomerAsync(request);
        }

        public async Task<ClientAmendResponse> DeleteCustomer(Guid id)
        {
            return await _customerService.DeleteCustomerAsync(id);
        }

        public async Task<ClientAmendResponse> EditCustomer(Guid id, CreateOrEditCustomerRequest request)
        {
            return await _customerService.EditCustomerAsync(id, request);
        }

        public async Task SubscribeCustomersInfo()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, GroupNames.Customers);
        }

        public async Task SubscribeCustomerInfo(Guid id)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, GroupNames.Customer(id));
        }
    }
}
