﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.SignalRServer
{
    public class GroupNames
    {
        public static string Customers => "Customers";

        public static string Customer(Guid id) => $"Customer-{id}";
    }
}
